import itertools as it
from random import shuffle
from tkinter import *

class SetKarte:
    """Repraesentiert eine Set-Karte"""

    def __init__(self, color, count, shape, pattern):
        self.col = color
        self.cnt = count
        self.shp = shape
        self.ptt = pattern


    def werBinIch(self):
        print ("hallo, ich bin eine SetKarte, naemlich (" + str(self.col) + ", " + str(self.cnt) + ", " + str(self.shp) + ", " + str(self.ptt) + ").")




def isSet(k1, k2, k3):
    """beurteilt, ob ein Dreiergespann von SetKarten ein Set ist"""
    colSum = (k1.col + k2.col + k3.col) % 3
    cntSum = (k1.cnt + k2.cnt + k3.cnt) % 3
    shpSum = (k1.shp + k2.shp + k3.shp) % 3
    pttSum = (k1.ptt + k2.ptt + k3.ptt) % 3

    return ([colSum, cntSum, shpSum, pttSum] == [0, 0, 0, 0])

class KartenVorrat:
    """Enthaelt alle moeglichen Set-Karten"""

    def __init__(self):
        self.cards = list(it.product(range(3), repeat = 4))

    def durchmischen(self):
        shuffle(self.cards)


#    """Wrappt einige Zeichenfunktionen von Tk"""
def erzeuge_rectangle(w, x0, y0, x1, y1, farbe):
    Canvas.create_rectangle(w, x0, y0, x1, y1, fill=farbe)

def erzeuge_raute(w, x0, y0, x1, y1, farbe):
    Canvas.create_polygon(w, x0, (y0 + y1) / 2,
                                (x0+x1)/2, y0,
                                x1, (y0 + y1)/2,
                                (x0+x1)/2, y1,
                            fill=farbe)

def erzeuge_oval(w, x0, y0, x1, y1, farbe):
    Canvas.create_oval(w, x0, y0, x1, y1, fill=farbe)

class SetKartenMaler:
    """Stellt die Set-Karte visuell dar!"""

    def __init__(self):
        #RotGrünBlau
        self.farbpalette = ["#b33", "#3b6", "#33b"]

        # RectangleDiamondOval
        self.formenpalette = [erzeuge_rectangle, erzeuge_raute, erzeuge_oval] # todo: fixme


        # Karten-Geometrie
        self.karte_width = 220
        self.karte_height = 335
        self.vmid = int(self.karte_height / 2)


    def maleKarte(self, k):
        #braucht man zum zeichnen
        master = Tk()

        w = Canvas(master,
                   width=self.karte_width,
                   height=self.karte_height)
        w.pack()

        # wähle zeichenparameter
        aktuelle_farbe = self.farbpalette[k.col]
        aktuelle_zeichenfunktion = self.formenpalette[k.shp]

        aktuelle_zeichenfunktion(w, 50,(self.vmid + 0) -30,170,(self.vmid + 0) +30, farbe=aktuelle_farbe)
        aktuelle_zeichenfunktion(w, 50,(self.vmid + self.karte_height*3/10) -30,170,(self.vmid + self.karte_height*3/10) +30, farbe=aktuelle_farbe)
        aktuelle_zeichenfunktion(w, 50,(self.vmid - self.karte_height*3/10) -30,170,(self.vmid - self.karte_height*3/10) +30, farbe= aktuelle_farbe)


# Ein paar Tests.

x = SetKarte(0, 1, 1, 1)
y = SetKarte(0, 1, 2, 2)
z = SetKarte(0, 1, 2, 0)
w = SetKarte(1, 1, 0, 0)
w2 = SetKarte(2, 1, 2, 0)
x.werBinIch()

kv = KartenVorrat()
#print (kv.cards)
#kv.durchmischen()
#print (kv.cards)

skm = SetKartenMaler()
#skm.maleKarte(x)
#SetKartenMaler.maleKarte(skm, x)
#skm.maleKarte(w)
#skm.maleKarte(w2)

for kartendefinition in kv.cards:
    sk = SetKarte(*kartendefinition)
    skm.maleKarte(sk)

#print(isSet(x,y,z))
#print(isSet(x,y,w))






mainloop()
